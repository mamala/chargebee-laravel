<?php


namespace Mamala\Chargebee;

use Illuminate\Support\ServiceProvider;

class ChargebeeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        if($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/chargebee.php' => $this->app->configPath('chargebee.php')
            ], 'chargebee-config');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/chargebee.php',
            'chargebee'
        );
    }
}